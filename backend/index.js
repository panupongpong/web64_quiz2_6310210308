const bcrypt = require('bcrypt')
const SALT_ROUNDS = 10

const jwt = require('jsonwebtoken')
const dotenv = require('dotenv')
dotenv.config()
const TOKEN_SECRET = process.env.TOKEN_SECRET

const mysql = require('mysql');

const connection = mysql.createConnection({
    host : 'localhost',
    user : 'root',
    password : 'root',
    database : 'HotelSystem'
});

connection.connect();

const express = require('express');
const app = express()
const port = 4000

/* Middleware for Authenticating User Token */
function authenticeteToken(req,res,next) {
    const authHeader = req.headers['authorization']
    const token = authHeader && authHeader.split(' ')[1]

    if (token == null) return res.sendStatus(401)
    jwt.verify(token, TOKEN_SECRET, (err,user) => {
        if (err) { return res.sendStatus(403) }
        else{
            req.user = user
            next()
        }
    })
}


app.get("/list_hotel", (req, res) => {
    let query = "SELECT * from Hotel";
    connection.query( query, (err, rows) => {
            if (err) {
                res.json({
                    "status" : "400",
                    "message" : "Error querying from running db"
                })
            }else {
                res.json(rows)
    }
    });
})

app.post("/add_Hotel", (req, res)  =>  {

    let hotel_name = req.query.hotel_name
    let hotel_location = req.query.hotel_location
    let hotel_provice = req.query.hotel_provice
    let hotel_grad = req.query.hotel_grad

    let query = `INSERT INTO Hotel
        (HotelName, HotelLocation, HotelProvince, HotelGrade)
        VALUES ('${hotel_name}','${hotel_location}',
                '${hotel_provice}','${hotel_grad}')`
    console.log(query)

    connection.query( query, (err, rows) => {
        console.log(err)
        if (err) {
            res.json({
                "status" : "400",
                "message" : "Error inserting data into db"
                    })
        }else {
            res.json({
                "status" : "200",
                "message" : "Adding event succesful"
            })
        }
    });
})

app.post("/update_hotel", (req, res)  =>  {
    
    let hotel_id = req.query.hotel_id
    let hotel_name = req.query.hotel_name
    let hotel_location = req.query.hotel_location
    let hotel_provice = req.query.hotel_provice
    let hotel_grad = req.query.hotel_grad

    let query = `UPDATE Hotel SET
                HotelName = '${hotel_name}',
                HotelLocation = '${hotel_location}',
                HotelProvince ='${hotel_provice}',
                HotelGrade= '${hotel_grad}'
            WHERE HotellD =  ${hotel_id}`

    console.log(query)

    connection.query( query, (err, rows) => {
        if (err) {
            console.log(err)
            res.json({
                "status" : "400",
                "message" : "Error updating data record"
                    })
        }else {
            res.json({
                "status" : "200",
                "message" : "Updating event succesful"
            })
        }
    });
})

app.post("/delete_hotel", (req, res)  =>  {
    
    let hotel_id = req.query.hotel_id
    let query = `DELETE FROM Hotel WHERE HotellD =  ${hotel_id}`

    console.log(query)

    connection.query( query, (err, rows) => {
        if (err) {
            console.log(err)
            res.json({
                "status" : "400",
                "message" : "Error deleting record"
                 })
        }else {
            res.json({
                "status" : "200",
                "message" : "Deleting event succesful"
         })
      }
    });
})

app.post("/register_person", (req,res) =>{
        let person_name  = req.query.person_name
        let person_surname = req.query.person_surname
        let person_gender = req.query.person_gender
        let person_old = req.query.person_old
        let person_username = req.query.person_username
        let person_password = req.query.person_password
    bcrypt.hash(person_password, SALT_ROUNDS, (err,hash)=>{
        let query = `INSERT INTO  Person
                 (PersonName, PersonSurname, PersonGender, PersonOld,  Username, Password )
                 VALUES ('${person_name}','${person_surname}',
                        '${person_gender}','${person_username}',
                        '${person_old}','${hash}')`
                console.log(query)

                connection.query( query,(err, rows) =>{
                    if (err) {
                        res.json({
                            "status" : "400",
                            "message" : "Error inserting data into db"
                        })
                    }else {
                        res.json ({
                                "status" : "200",
                                "message" : "Adding new user succesful"
                        })
                    }
                });   

    })
});

app.post("/register_hotel", authenticeteToken, (req, res)  =>  {
    let user_profile = req.user
    let person_id = req.user.user_id
    let hotel_id = req.query.hotel_id
    let chamber = req.query.chamber
    let quantity = req.query.quantity
    let query = `INSERT INTO Registration
                (PersonID, HotelID, Chamber, Quantity, RegistrationTime)
                VALUES ('${person_id}',
                        '${hotel_id}',
                        '${chamber}',
                        '${quantity}',
                        NOW() )`
    console.log(query)

    connection.query( query, (err, rows) => {
        if (err) {
            res.json({
                "status" : "400",
                "message" : "Error inserting data into db"
                   })
        }else {
            res.json({
                "status" : "200",
                "message" : "Adding event succesful"
            })
        }
    });
})

/*SELECT Person.PersonID, Hotel.HotelName,Registration.Chamber,RegistrationTime
FROM Person, Hotel,Registration
WHERE (Person.PersonID = Registration.PersonID)AND
(Registration.HotelID = Hotel.HotellD)AND
(Person.PersonID = 2 ) */

app.post("/list_reg_by_personid", authenticeteToken, (req, res) => {
    let person_id = req.user.user_id
    let query = `
    SELECT Person.PersonID, Hotel.HotelName,Registration.Chamber,RegistrationTime
    FROM Person, Hotel,Registration
    WHERE (Person.PersonID = Registration.PersonID)AND
    (Registration.HotelID = Hotel.HotellD)AND
    (Person.PersonID = ${person_id});`;
connection.query( query, (err, rows) => {
     if (err) {
        console.log(err)
         res.json({
                "status" : "400",
                "message" : "Error querying from running db"
            })
     }else {
        res.json(rows)
}
});

})

app.post("/login", (req, res) =>{
    let username = req.query.username
    let user_password = req.query.password
    let query = `SELECT * FROM Person WHERE Username = '${username}'`
    connection.query( query, (err,rows) =>{
                if (err) {
                    console.log(err)
                    res.json({
                        "status" : "400",
                        "message" : "Error querying from running db"
                        })
                }else {
                    let db_password = rows[0].Password
                    bcrypt.compare(user_password, db_password, (err, result) =>{
                        if (result){
                          let payload = {
                              "username" : rows[0].Username,
                              "user_id" : rows[0].PersonID,
                          }
                          console.log(payload)
                          let token = jwt.sign(payload, TOKEN_SECRET, {expiresIn : '1d'})
                          res.send(token)
                        }else { res.send ("Invalid username / password")}
                    })
                    
                } 
    })
})

app.listen(port, () => {
console.log(`Now starting Hotel System Beckend ${port}`)
})

